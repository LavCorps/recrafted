-- replacement for default gps

local args = { ... }

local gps = require("gps")
local term = require("term")
local peripheral = require("peripheral")
local rc = require("rc")

if #args < 1 then
    error("incorrect invocation")
end

local cmd = args[1]

if cmd == "locate" then
    print("working...")
    pos = gps.locate(1)
    if pos then
        print("coords are " .. tostring(pos.x) .. ", " .. tostring(pos.y) .. ", " .. tostring(pos.z))
    else
        print("unable to trilaterate (try changing positions)")
    end
elseif cmd == "host" then
    local modem
    local x, y, z

    local tW, tH = term.getSize()
    if tW == 26 and tH == 20 then
        error("host cannot be a pocket computer")
    end

    local modems = { peripheral.find("modem") }
    for _, m in pairs(modems) do
        if m.isWireless() then
            modem = m
        end
    end
    if modem == nil then
        error("no wireless modem found")
    end

    if args[2] and args[3] and args[4] then
        x = tonumber(args[2])
        y = tonumber(args[3])
        z = tonumber(args[4])
    else
        x, y, z = gps.locate()
    end
    if x == nil or y == nil or z == nil then
        error("please provide x, y, and z coords as args")
    end

    print("opening gps channel")
    modem.open(gps.CHANNEL_GPS)

    while true do
        local _, _, ch, rch, msg, dst = rc.pullEvent("modem_message")
        if ch == gps.CHANNEL_GPS and msg == "PING" and dst then
            modem.transmit(rch, gps.CHANNEL_GPS, { x, y, z })
            print("[" .. tostring(os.clock()) .. "] request resolved")
        end
    end
else
    error("use gps host or gps locate")
end
