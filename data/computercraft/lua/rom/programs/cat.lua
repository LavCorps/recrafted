-- concatenate files to stdout

local args = { ... }

local shell = require("shell")

for _, file in ipairs(args) do
    for line in io.lines(shell.resolve(file)) do
        print(line)
    end
end
