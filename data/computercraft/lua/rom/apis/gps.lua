-- rc.gps

local alarm = require("alarm")
local expect = require("cc.expect").expect
local peripheral = require("peripheral")
local rc = require("rc")
local vector = require("vector")

local gps = {}
gps.CHANNEL_GPS = 65534

local function cmb(tabs, k, curr, out)
    if #curr == 3 then
        table.insert(out, { table.unpack(curr) })
        return
    end
    for i = k, #tabs do
        table.insert(curr, tabs[i])
        cmb(tabs, i + 1, curr, out)
        table.remove(curr)
    end
end

local function trilat(ctcs) -- TODO: improve checks against bad satellite sets
    if #ctcs < 3 then
        return nil
    elseif #ctcs == 3 then
        local dst = (ctcs[2].pos - ctcs[1].pos):length()
        local vca = (ctcs[2].pos - ctcs[1].pos):normalize()
        local sca = vca:dot(ctcs[3].pos - ctcs[1].pos)
        local vcb = ((ctcs[3].pos - ctcs[1].pos) - vca * sca):normalize()
        local scb = vcb:dot(ctcs[3].pos - ctcs[1].pos)
        local vcc = vca:cross(vcb)

        local x = (ctcs[1].dst ^ 2 - ctcs[2].dst ^ 2 + dst ^ 2) / (dst * 2)
        local y = (ctcs[1].dst ^ 2 - ctcs[3].dst ^ 2 - x ^ 2 + (x - sca) ^ 2 + scb ^ 2) / (scb * 2)
        local z = math.sqrt(ctcs[1].dst ^ 2 - x ^ 2 - y ^ 2)

        local out = ctcs[1].pos + vca * x + vcb * y + vcc * z
        return out
    elseif #ctcs > 3 then
        local cmbo = {}
        cmb(ctcs, 1, {}, cmbo)

        local tri = {}
        local trh = {}
        local trm = { str = "", cnt = 0 }
        for _, x in ipairs(cmbo) do
            if
                x[1].dst + x[2].dst > x[3].dst
                and x[1].dst + x[3].dst > x[2].dst
                and x[3].dst + x[2].dst > x[1].dst
                and x[1].dst > 0
                and x[2].dst > 0
                and x[3].dst > 0
                and ((x[1].pos.x == x[2].pos.x and x[1].pos.x == x[3].pos.x) or (x[1].pos.y == x[2].pos.y and x[1].pos.y == x[3].pos.y) or (x[1].pos.z == x[2].pos.z and x[1].pos.z == x[3].pos.z))
                and math.abs((x[2].pos - x[1].pos):normalize():dot((x[3].pos - x[1].pos):normalize())) < 1
            then
                local y = trilat(x)
                if (y.x ~= y.x or y.y ~= y.y or y.z ~= y.z) then
                else
                    table.insert(tri, y)
                    if trh[tostring(y)] then
                        trh[tostring(y)] = trh[tostring(y)] + 1
                    else
                        trh[tostring(y)] = 1
                    end
                end
            elseif x[1].dst == 0 then
                return x[1].pos
            elseif x[2].dst == 0 then
                return x[2].pos
            elseif x[3].dst == 0 then
                return x[3].pos
            elseif #ctcs == 3 then
                return nil
            end
        end
        if #tri == 0 then
            return nil
        end
        for str, cnt in pairs(trh) do
            if cnt > trm.cnt then
                trm.str = str
                trm.cnt = cnt
            end
        end
        for _, vct in pairs(tri) do
            if tostring(vct) == trm.str then
                return vct
            end
        end
        return nil
    end
end

function gps.locate(timeout)
    timeout = expect(1, timeout, "number", "nil") or 2

    local modem
    local ctcs = {}
    local out

    local modems = { peripheral.find("modem") }
    for _, m in pairs(modems) do
        if m.isWireless() then
            modem = m
        end
    end
    if modem == nil then
        return nil
    end

    modem.open(gps.CHANNEL_GPS)
    modem.transmit(gps.CHANNEL_GPS, gps.CHANNEL_GPS, "PING")

    local timer = alarm.alarm(timeout)
    local halftime = {timer = alarm.alarm(timeout/2), hit = false}
    while true do
        local e, t, ch, rch, msg, dst = rc.pullEvent()
        if e == "modem_message" then
            if ch == gps.CHANNEL_GPS and rch == gps.CHANNEL_GPS and dst then
                if
                    type(msg) == "table"
                    and #msg == 3
                    and tonumber(msg[1])
                    and tonumber(msg[2])
                    and tonumber(msg[3])
                then
                    table.insert(
                        ctcs,
                        { pos = vector.new(tonumber(msg[1]), tonumber(msg[2]), tonumber(msg[3])), dst = dst }
                    )
                    if halftime.hit then
                        out = trilat(ctcs)
                        if out then
                            break
                        end
                    end
                end
            end
        elseif e == "alarm" then
            if t == halftime.timer then
                out = trilat(ctcs)
                if out then
                    break
                end
                halftime.hit = true
            elseif t == timer then
                break
            end
        end
    end
    modem.close(gps.CHANNEL_GPS)
    if out then
        return out:round(0.001)
    else
        return nil
    end
end

return gps
