-- rc.alarm

local rc = require("rc")
local thread = require("rc.thread")

local alarm = { _TOUTIN = 0 }

function alarm.alarm(time)
    alarm._TOUTIN = alarm._TOUTIN + 1
    local i = alarm._TOUTIN
    local function alarm_internal()
        rc.sleep(time)
        rc.queueEvent("alarm", i)
    end
    thread.spawn(alarm_internal, "alarm-" .. tostring(alarm._TOUTIN))
    return alarm._TOUTIN
end

return alarm
