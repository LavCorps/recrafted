local shell = require("shell")
local completion = require("cc.shell.completion")

shell.setCompletionFunction("cat", completion.build({ completion.file, many = true }))
