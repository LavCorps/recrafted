local shell = require("shell")
local completion = require("cc.shell.completion")

shell.setCompletionFunction(
    "gps",
    completion.build(
        { completion.choice, { "host", "locate" } }
    )
)
